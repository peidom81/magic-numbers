import '../../bloc/bloc.dart';
import 'magic_numbers_initial.dart';
import 'package:flutter/material.dart';
import 'magic_numbers_unmemorized.dart';
import 'magic_numbers_memorized.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MagicNumbersStateHandler extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final MagicCardsBloc magicCardsBloc = BlocProvider.of<MagicCardsBloc>(context);
    return BlocBuilder<MagicCardsBloc, MagicCardsState>(
      bloc: magicCardsBloc,
      builder: (BuildContext context, MagicCardsState state) {
        if(state is InitialMagicCardsState) {
          return MagicNumbersInitialState();
        }
        if(state is UnmemorizedMagicCardsState) {
          return MagicNumbersUnmemorizedState(randomMagicCardId: state.randomMagicCardId);
        }
        if(state is MemorizedMagicCardsState) {
          return MagicNumbersMemorizedState();
        }
        return MagicNumbersInitialState();
      },
    );
  }
}
