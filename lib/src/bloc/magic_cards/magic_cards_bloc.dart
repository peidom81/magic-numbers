import 'dart:async';
import 'package:bloc/bloc.dart';
import '../../models/models.dart';
import '../bloc.dart';
import 'dart:math';

class MagicCardsBloc extends Bloc<MagicCardsEvent, MagicCardsState> {
  @override
  MagicCardsState get initialState => InitialMagicCardsState();

  int getRandomMagicCardId() {
    Random randomValue = Random();
    int randomMagicCardId = randomValue.nextInt(6);
    return randomMagicCardId;
  }

  @override
  Stream<MagicCardsState> mapEventToState(MagicCardsEvent event) async* {
    if(event is StartMagicCardsEvent) {
      yield* _mapStartMagicCardsEventToState();
    }
    if (event is MemorizedNumberEvent) {
      yield* _mapMemorizedNumberEventEventToState(event);
    }
  }

  Stream<MagicCardsState> _mapStartMagicCardsEventToState() async* {
    int randomMagicCardId = getRandomMagicCardId();
    yield UnmemorizedMagicCardsState(randomMagicCardId: randomMagicCardId);
  }

  Stream<MagicCardsState> _mapMemorizedNumberEventEventToState(MemorizedNumberEvent event) async* {
    int memorizedCardId = event.cardId;
    yield MemorizedMagicCardsState(memorizedCardId: memorizedCardId);
  }
}
