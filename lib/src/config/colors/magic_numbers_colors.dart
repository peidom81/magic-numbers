import 'package:flutter/material.dart';

class MagicNumbersColors {
  static const Color black = Color(0xFF000000);
  static const Color gold = Color(0xFFFED100);
  static const Color green = Color(0xFF005514);
  static const Color red = Color(0xFFBD040D);
  static const Color lightGreen = Color(0xFF009B3A);
}