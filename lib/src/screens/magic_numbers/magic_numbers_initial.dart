import 'package:flutter/material.dart';
import 'package:magic_numbers/src/config/colors/magic_numbers_colors.dart';
import '../../bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MagicNumbersInitialState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final MagicCardsBloc magicCardsBloc = BlocProvider.of<MagicCardsBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Magic Numbers',
        ),
      ),
      body: Container(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                child: Text('Quer ver um truque?'),
              ),
              Container(
                child: RaisedButton(
                  color: MagicNumbersColors.lightGreen,
                  child: Text('Vamos lá!', style: TextStyle(color: Colors.white)),
                  onPressed: () {
                    magicCardsBloc.dispatch(StartMagicCardsEvent());
                  },
                ),
              ),
            ],
          )
      ),
    );
  }
}
