import 'package:flutter/material.dart';
import '../../config/config.dart';

class Welcome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Wellcome Page'),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  Text('Welcome', style: TextStyle(color: MagicNumbersColors.black),),
                  RaisedButton(
                    child: Text('MagicNumbers'),
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, '/magic-numbers');
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
