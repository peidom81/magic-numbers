import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import '../../models/models.dart';

@immutable
abstract class MagicCardsState extends Equatable {
  MagicCardsState([List props = const []]) : super(props);
}

class InitialMagicCardsState extends MagicCardsState {}

class UnmemorizedMagicCardsState extends MagicCardsState {
  final int randomMagicCardId;
  UnmemorizedMagicCardsState({this.randomMagicCardId}) : super([randomMagicCardId]);
}

class MemorizedMagicCardsState extends MagicCardsState {
  final int memorizedCardId;
  MemorizedMagicCardsState({this.memorizedCardId}) : super([memorizedCardId]);
}