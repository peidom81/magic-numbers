import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:magic_numbers/src/bloc/bloc.dart';
import 'package:magic_numbers/src/config/colors/magic_numbers_colors.dart';
import './screens/magic_numbers/magic_numbers_state_handler.dart';
import './screens/welcome/welcome.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Magic Numbers',
      theme: ThemeData(
        primarySwatch: Colors.amber,
        primaryColor: MagicNumbersColors.green,
        accentColor: MagicNumbersColors.gold,
        cursorColor: MagicNumbersColors.red,
      ),
      onGenerateRoute: onGenerateRoute,
    );
  }

  MaterialPageRoute onGenerateRoute(RouteSettings settings) {
    if (settings.name == '/magic-numbers') {
      return MaterialPageRoute(
        builder: (BuildContext context) {
          return BlocProvider(
            builder: (BuildContext context)=> MagicCardsBloc(),
            child: MagicNumbersStateHandler(),
          );
        }
      );
    }
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return Welcome();
      }
    );
  }
}
