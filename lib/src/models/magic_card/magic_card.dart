import '../models.dart';

class MagicCard {
  final int id;
  // final List<CardRow> rows;
  final List<int> rows;
  const MagicCard({this.id, this.rows});
}