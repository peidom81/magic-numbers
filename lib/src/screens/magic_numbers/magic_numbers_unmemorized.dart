import 'package:flutter/material.dart';
import 'package:magic_numbers/src/config/colors/magic_numbers_colors.dart';
import '../../bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MagicNumbersUnmemorizedState extends StatelessWidget {
  final int randomMagicCardId;

  MagicNumbersUnmemorizedState({
    @required this.randomMagicCardId,
  });

  @override
  Widget build(BuildContext context) {
    final MagicCardsBloc magicCardsBloc =
        BlocProvider.of<MagicCardsBloc>(context);

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.clear),
          onPressed: () => Navigator.pushReplacementNamed(context, 'welcome'),
        ),
        title: Text(
          'Magic Numbers',
        ),
      ),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              child: Text('Memorize um número!'),
            ),
            Text('$randomMagicCardId'),
            Container(
              child: RaisedButton(
                color: MagicNumbersColors.lightGreen,
                child: Text('Pronto', style: TextStyle(color: Colors.white)),
                onPressed: () {
                  magicCardsBloc.dispatch(MemorizedNumberEvent(cardId: randomMagicCardId));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
