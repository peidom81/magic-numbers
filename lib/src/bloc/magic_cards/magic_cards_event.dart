import 'package:magic_numbers/src/models/magic_card/magic_card.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class MagicCardsEvent extends Equatable {
  MagicCardsEvent([List props = const []]) : super(props);
}

class StartMagicCardsEvent extends MagicCardsEvent {}

class MemorizedNumberEvent extends MagicCardsEvent {
  final int cardId;
  MemorizedNumberEvent({this.cardId}) : super([cardId]);
}
